/* gcc -o wgnl_test -O2 wgnl_test.c -lmnl */

#include <libmnl/libmnl.h>

#include <linux/netlink.h>
#include <linux/genetlink.h>
#include <linux/time_types.h>
#include <linux/wireguard.h>

#ifndef WG_MULTICAST_GROUP_PEER_CHANGE
#define WG_MULTICAST_GROUP_PEER_CHANGE  "wg_peer_changed"
#endif

#ifndef WG_CMD_CHANGED_PEER
#define WG_CMD_CHANGED_PEER (WG_CMD_SET_DEVICE+1)
#endif

#include <assert.h>
#include <errno.h>
#include <poll.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define WG_DEVICE         "wg0"
#define SOCKET_BUFFER_SIZE 4096

struct nl_wg {
    uint32_t wg_peer_changed;
    uint16_t wg_family;
};

static int genl_family_attr_cb(const struct nlattr *attr, void *data) {
    const struct nlattr **tb = data;
    uint16_t type;

    if (mnl_attr_type_valid(attr, CTRL_ATTR_MAX) < 0) {
        printf("genl_family_attr_cb: mnl_attr_type_valid error\n");
        return MNL_CB_ERROR;
    }

    type = mnl_attr_get_type(attr);
    switch (type) {
        case CTRL_ATTR_FAMILY_NAME:
            if (mnl_attr_validate(attr, MNL_TYPE_STRING) < 0) {
                printf("genl_family_attr_cb: CTRL_ATTR_FAMILY_NAME invalid\n");
                return MNL_CB_ERROR;
            }
            if (strcmp(WG_GENL_NAME, mnl_attr_get_str(attr))) {
                printf("genl_family_attr_cb: Wrong FAMILY_NAME reply\n");
                return MNL_CB_ERROR;
            }
            break;
        case CTRL_ATTR_FAMILY_ID:
            if (mnl_attr_validate(attr, MNL_TYPE_U16) < 0) {
                printf("genl_family_attr_cb: CTRL_ATTR_FAMILY_ID invalid\n");
                return MNL_CB_ERROR;
            }
            break;
        case CTRL_ATTR_MCAST_GROUPS:
            if (mnl_attr_validate(attr, MNL_TYPE_NESTED) < 0) {
                printf("genl_family_attr_cb: CTRL_ATTR_MCAST_GROUPS invalid\n");
                return MNL_CB_ERROR;
            }
            break;
        default:
            break;
    }

    tb[type] = attr;
    return MNL_CB_OK;
}

static int genl_family_cb(const struct nlmsghdr *nlh, void *data) {
    struct nl_wg *nlwg = data;
    struct genlmsghdr *genl = mnl_nlmsg_get_payload(nlh);
    int err;

    if (nlh->nlmsg_type == GENL_ID_CTRL) {
        struct nlattr *tb[CTRL_ATTR_MAX+1] = {};

        err = mnl_attr_parse(nlh, sizeof(*genl), genl_family_attr_cb, tb);
        if (err != MNL_CB_OK) {
            return err;
        }
        if (tb[CTRL_ATTR_FAMILY_ID]) {
            nlwg->wg_family = mnl_attr_get_u16(tb[CTRL_ATTR_FAMILY_ID]);
        }
        if (tb[CTRL_ATTR_MCAST_GROUPS]) {
            struct nlattr *nested = tb[CTRL_ATTR_MCAST_GROUPS];
            const struct nlattr *gpos, *pos;

            mnl_attr_for_each_nested(gpos, nested) {
                const char *mc_name=NULL;
                uint32_t mc_id=0;

                mnl_attr_for_each_nested(pos, gpos) {
                    uint16_t type = mnl_attr_get_type(pos);

                    if (mnl_attr_type_valid(pos, CTRL_ATTR_MCAST_GRP_MAX) < 0) {
						printf("genl_family_cb: mnl_attr_type_valid() returned an error.\n");
                        return MNL_CB_ERROR;
                    }

                    switch(type) {
                        case CTRL_ATTR_MCAST_GRP_ID:
                            if (mnl_attr_validate(pos, MNL_TYPE_U32) < 0) {
						        printf("genl_family_cb: CTRL_ATTR_MCAST_GRP_ID invalid.\n");
                                return MNL_CB_ERROR;
                            }
                            mc_id = mnl_attr_get_u32(pos);
                            break;
                        case CTRL_ATTR_MCAST_GRP_NAME:
                            if (mnl_attr_validate(pos, MNL_TYPE_STRING) < 0) {
                                printf("genl_family_cb: CTRL_ATTR_MCAST_GRP_NAME invalid.\n");
                                return MNL_CB_ERROR;
                            }
                            mc_name = mnl_attr_get_str(pos);
                            break;
                        default:
                            break;
                    }
                }

                if (mc_name && !strcmp(WG_MULTICAST_GROUP_PEER_CHANGE, mc_name)) {
                    nlwg->wg_peer_changed = mc_id;
                    return MNL_CB_OK;
                }
            }
        }
    } else {
        printf("genl_family_cb: Surprised by nlh->nlmsg_type == %u\n", nlh->nlmsg_type);
    }
    return MNL_CB_OK;
}

static void getWGFamily(struct mnl_socket *nl, struct nl_wg *nlwg) {
    struct nlmsghdr *nlh;
    struct genlmsghdr *genl;
    char buf[SOCKET_BUFFER_SIZE];
    int n;
    time_t seq;

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type  = GENL_ID_CTRL;
    nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK;
    nlh->nlmsg_seq = seq = time(NULL);

    genl = mnl_nlmsg_put_extra_header(nlh, sizeof(struct genlmsghdr));
    genl->cmd = CTRL_CMD_GETFAMILY;
    genl->version = 1;

    mnl_attr_put_u16(nlh, CTRL_ATTR_FAMILY_ID, GENL_ID_CTRL);
    mnl_attr_put_strz(nlh, CTRL_ATTR_FAMILY_NAME, "wireguard");
    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0) {
        printf("getWGFamily: mnl_socket_sendto failed\n");
        exit(3);
    }

    n = mnl_socket_recvfrom(nl, buf, sizeof(buf));
    while (n > 0) {
        n = mnl_cb_run(buf, n, seq, mnl_socket_get_portid(nl), genl_family_cb, nlwg);
        if (n <= MNL_CB_STOP)
            break;
        n = mnl_socket_recvfrom(nl, buf, sizeof(buf));
    }
}

static int genl_wg_peer_cb(const struct nlattr *attr, void *data) {
    const struct nlattr **tb = data;
    uint16_t type;

    if (mnl_attr_type_valid(attr, WGDEVICE_A_MAX) < 0) {
        printf("genl_wg_peer_cb: attr higher than WGDEVICE_A_MAX\n");
        return MNL_CB_ERROR;
    }

    type = mnl_attr_get_type(attr);
    switch (type) {
    case WGDEVICE_A_IFINDEX:
        if (mnl_attr_validate(attr, MNL_TYPE_U32) < 0) {
            printf("genl_wg_peer_cb: WGDEVICE_A_IFINDEX invalid\n");
            return MNL_CB_ERROR;
        }
        break;
    case WGDEVICE_A_IFNAME:
        if (mnl_attr_validate(attr, MNL_TYPE_STRING) < 0) {
            printf("genl_wg_peer_cb: WGDEVICE_A_IFNAME invalid\n");
            return MNL_CB_ERROR;
        }
        break;
    case WGDEVICE_A_PEERS:
        if (mnl_attr_validate(attr, MNL_TYPE_NESTED) < 0) {
            printf("genl_wg_peer_cb: WGDEVICE_A_PEERS invalid\n");
            return MNL_CB_ERROR;
        }

        break;
    default:
        printf("genl_wg_peer_cb: default case?!\n");
    }
    tb[type] = attr;

    return MNL_CB_OK;
}

static int wg_peer_changed_cb(const struct nlmsghdr *nlh, void *data) {
    struct genlmsghdr *genl = mnl_nlmsg_get_payload(nlh);
    struct nl_wg *nlwg = data;
    int err;

    if (nlh->nlmsg_type != nlwg->wg_family) {
        printf("wg_peer_changed_cb: unknown nlmsg_type == %x\n", (unsigned)nlh->nlmsg_type);
        exit(40);
    }

    if (genl->cmd == WG_CMD_CHANGED_PEER) {
        struct nlattr *tb[WGDEVICE_A_MAX+1] = {};
        struct sockaddr_in  *addr4 = NULL;
        struct sockaddr_in6 *addr6 = NULL;

        err = mnl_attr_parse(nlh, sizeof(*genl), genl_wg_peer_cb, tb);
        if (err != MNL_CB_OK) {
            printf("wg_peer_changed_cb: error from mnl_attr_parse\n");
            return err;
        }

        printf("\nReceived WG_CMD_CHANGED_PEER message...\n");
        if (tb[WGDEVICE_A_IFINDEX]) {
            printf("Interface index: %u\n", mnl_attr_get_u32(tb[WGDEVICE_A_IFINDEX]));
        }
        if (tb[WGDEVICE_A_IFNAME]) {
            printf("Interface name: %s\n", mnl_attr_get_str(tb[WGDEVICE_A_IFNAME]));
        }
        if (tb[WGDEVICE_A_PEERS]) {
            const struct nlattr *pos, *peer = tb[WGDEVICE_A_PEERS];
            peer = mnl_attr_get_payload(peer);

            if (peer->nla_type != NLA_F_NESTED) {
                printf("wg_peer_changed_cb: WGDEVICE_A_PEERS does not have nested type zero attribute inside.");
                return MNL_CB_ERROR;
            }

            mnl_attr_for_each_nested(pos, peer) {
                uint16_t type = mnl_attr_get_type(pos);

                if (mnl_attr_type_valid(pos, WGPEER_A_MAX) < 0) {
                    printf("wg_peer_changed_cb: nested attribute invalid\n");
                    return MNL_CB_ERROR;
                }
                switch (type) {
                case WGPEER_A_PUBLIC_KEY:
                    if (WG_KEY_LEN == mnl_attr_get_payload_len(pos)) {
						printf("Got the public key\n");
                    } else {
                        printf("wg_peer_changed_cb: wrong size for WGPEER_A_PUBLIC_KEY!\n");
                    }
                    break;
                case WGPEER_A_ENDPOINT:
                    if (sizeof(*addr4) == mnl_attr_get_payload_len(pos)) {
                        addr4 = mnl_attr_get_payload(pos);
						printf("Got IPv4 address: %s Port: %d\n", inet_ntoa(addr4->sin_addr), ntohs(addr4->sin_port));
                    } else if (sizeof(*addr6) == mnl_attr_get_payload_len(pos)) {
                        addr6 = mnl_attr_get_payload(pos);
                        printf("Got an IPv6 address!\n");
                    } else {
                        printf("wg_peer_changed_cb: unknown size of WGPEER_A_ENDPOINT %u (instead of %lu)!\n", mnl_attr_get_payload_len(pos), sizeof(addr4));
                    }
                    break;
                default:
                    break;
                }
            }
        }
    } else {
        printf("wg_peer_changed_cb: unknown nlmsg_type %x\n", (unsigned)nlh->nlmsg_type);
        return MNL_CB_ERROR;
    }
    return MNL_CB_OK;
}


int main(int argc, char *argv[]) {
    char buf[SOCKET_BUFFER_SIZE];
    struct mnl_socket *nl;
    int rv, n;
    struct nl_wg nlwg;

    //assert(MNL_SOCKET_BUFFER_SIZE == SOCKET_BUFFER_SIZE);

    nl = mnl_socket_open(NETLINK_GENERIC);
    if (nl == NULL) {
        printf("Error from mnl_socket_open failed()\n");
        return 1;
    }

    if (mnl_socket_bind(nl, 0, MNL_SOCKET_AUTOPID) < 0) {
        printf("Error from mnl_socket_bind()\n");
        return 2;
    }

    getWGFamily(nl, &nlwg);

    if (mnl_socket_setsockopt(nl, NETLINK_ADD_MEMBERSHIP, &nlwg.wg_peer_changed, sizeof(nlwg.wg_peer_changed)) != 0) {
        printf("Error from mnl_socket_setsockopt()\n");
        return 4;
    }

    do {
        n = mnl_socket_recvfrom(nl, buf, sizeof(buf));
        n = mnl_cb_run(buf, n, 0, 0, wg_peer_changed_cb, &nlwg);
        if (n < 0) {
            printf("Error from mnl_cb_run!\n");
			mnl_socket_close(nl);
			exit(5);
        }
    } while (1);

    mnl_socket_close(nl);
    return 0;
}
